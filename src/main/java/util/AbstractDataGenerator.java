package util;

import com.opencsv.CSVWriter;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public abstract class AbstractDataGenerator {

    protected abstract void generate(int noOfEntries,String outputPath);
   //Util class methods should be static, as we dont want to instantiate util classes.
    protected static CSVWriter getWriter(File file, String fileName) throws IOException {

        File newFile = new File (file, fileName);
        FileWriter outputfile = new FileWriter(newFile);
        CSVWriter writer = new CSVWriter(outputfile);

        return writer;

    }

    public static void copyFilesFromLocal(String localPath, String hdfsPath) {

        Configuration conf = new Configuration();

        FileSystem fs = null;
        try {
            fs = FileSystem.get(conf);
            fs.copyFromLocalFile(new Path(localPath),
                    new Path(hdfsPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
