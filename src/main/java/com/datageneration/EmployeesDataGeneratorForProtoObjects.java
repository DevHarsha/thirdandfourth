package com.datageneration;

import com.github.javafaker.Faker;
import com.opencsv.CSVWriter;
import com.protoobjects.Employee;
import util.AbstractDataGenerator;

import java.io.File;
import java.io.IOException;

//Generates Employee and Building Data
public class EmployeesDataGeneratorForProtoObjects extends AbstractDataGenerator {

    private int TOTAL_BUILDINGS;
    private static final String employees = "Employee.csv";

    public EmployeesDataGeneratorForProtoObjects(int total_buildings){
        this.TOTAL_BUILDINGS = total_buildings;
    }

    @Override
    public void generate(int noOfEntries,String outputPath) {

        Faker fake;
        String[] employee_header = {"name","employee_id", "building_code", "floor_number", "salary", "department"};

        try {

            File dir = new File (outputPath);

            CSVWriter employeeWriter = getWriter(dir,employees);
            Employee.FLOOR_NUMBER number;

            employeeWriter.writeNext(employee_header);

            //name, employee_id, building_code, floor_number (this should be enum), salary, department
            for(int i=0;i<noOfEntries;i++){

                fake  = new Faker();
                number = Employee.FLOOR_NUMBER.forNumber(i%6);
                String[] employee_data = {fake.name().fullName(), String.valueOf(fake.hashCode()),
                        String.valueOf(i%TOTAL_BUILDINGS), String.valueOf(number),String.valueOf(fake.hashCode()),fake.commerce().department()};
                employeeWriter.writeNext(employee_data);

            }

            employeeWriter.close();

        }
        catch (IOException e) {
            e.printStackTrace();
        }


    }



}
