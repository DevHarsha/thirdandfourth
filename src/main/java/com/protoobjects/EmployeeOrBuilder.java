// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Employee.proto

package com.protoobjects;

public interface EmployeeOrBuilder extends
    // @@protoc_insertion_point(interface_extends:third_assignment.Employee)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>required string name = 1;</code>
   * @return Whether the name field is set.
   */
  boolean hasName();
  /**
   * <code>required string name = 1;</code>
   * @return The name.
   */
  java.lang.String getName();
  /**
   * <code>required string name = 1;</code>
   * @return The bytes for name.
   */
  com.google.protobuf.ByteString
      getNameBytes();

  /**
   * <code>required int32 employee_id = 2;</code>
   * @return Whether the employeeId field is set.
   */
  boolean hasEmployeeId();
  /**
   * <code>required int32 employee_id = 2;</code>
   * @return The employeeId.
   */
  int getEmployeeId();

  /**
   * <code>required int32 building_code = 3;</code>
   * @return Whether the buildingCode field is set.
   */
  boolean hasBuildingCode();
  /**
   * <code>required int32 building_code = 3;</code>
   * @return The buildingCode.
   */
  int getBuildingCode();

  /**
   * <code>optional .third_assignment.Employee.FLOOR_NUMBER floor = 4 [default = GROUND];</code>
   * @return Whether the floor field is set.
   */
  boolean hasFloor();
  /**
   * <code>optional .third_assignment.Employee.FLOOR_NUMBER floor = 4 [default = GROUND];</code>
   * @return The floor.
   */
  Employee.FLOOR_NUMBER getFloor();

  /**
   * <code>optional int32 salary = 5;</code>
   * @return Whether the salary field is set.
   */
  boolean hasSalary();
  /**
   * <code>optional int32 salary = 5;</code>
   * @return The salary.
   */
  int getSalary();

  /**
   * <code>required string department = 6;</code>
   * @return Whether the department field is set.
   */
  boolean hasDepartment();
  /**
   * <code>required string department = 6;</code>
   * @return The department.
   */
  java.lang.String getDepartment();
  /**
   * <code>required string department = 6;</code>
   * @return The bytes for department.
   */
  com.google.protobuf.ByteString
      getDepartmentBytes();

  /**
   * <code>optional int32 cafeteria_code = 7;</code>
   * @return Whether the cafeteriaCode field is set.
   */
  boolean hasCafeteriaCode();
  /**
   * <code>optional int32 cafeteria_code = 7;</code>
   * @return The cafeteriaCode.
   */
  int getCafeteriaCode();
}
