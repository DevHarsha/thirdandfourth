package com.protoassignments.third;

import com.protoobjects.Building;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile.Reader;

import java.io.IOException;
import java.util.HashMap;

public class BuildHashMap {

    protected HashMap<String, String> getHashMap(String path) {

        HashMap<String, String> LookUpTable = new HashMap<>();
        Configuration conf = new Configuration();
        Reader reader = null;
        try {
            Path inFile = new Path(path);
            reader = new Reader(conf, Reader.file(inFile), Reader.bufferSize(4096));
            populateHashMap(LookUpTable, reader);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                IOUtils.closeStream(reader);
            }
        }
        return LookUpTable;

    }

    private void populateHashMap(HashMap<String, String> buildingMap, Reader reader) throws IOException {

        IntWritable key = new IntWritable();
        ImmutableBytesWritable value = new ImmutableBytesWritable();

        while (reader.next(key, value)) {
            Building.Builder building = Building.newBuilder().
                    mergeFrom(value.get());
            int building_code = Integer.parseInt(String.valueOf(building.getBuildingCode()));
            String cafeteria_code = String.valueOf(building.getCafeteriaCode());
            buildingMap.put(String.valueOf(building_code), cafeteria_code);
        }

    }

}
