package com.protoassignments.third;

import com.protoobjects.Employee;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableSplit;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

public class EmployeeCafeteriaMapper extends TableMapper<LongWritable, ImmutableBytesWritable> {

    private byte[] EMPLOYEES_TABLE = Bytes.toBytes("employees");
    private byte[] CF_NAME = Bytes.toBytes("information");

    private byte[][] QUAL_BYTES = {"name".getBytes(), "employee_id".getBytes(),"building_code".getBytes(),
            "floor_number".getBytes(),"salary".getBytes(),
            "department".getBytes()};

    private String path_to_building = "/user/DevHarsha/ProtoSequence/building.seq";
    private HashMap<String, String> LookUpTable;

    @Override
    protected void setup(Context context) {

        BuildHashMap buildHashMap = new BuildHashMap();
        LookUpTable = buildHashMap.getHashMap(path_to_building);
        System.out.println("Lookup table : " + LookUpTable);

    }

    @Override
    public void map(ImmutableBytesWritable key, Result result, Context context) {

        TableSplit tableSplit = (TableSplit) context.getInputSplit();
        byte[] table = tableSplit.getTableName();

        try {
            if (Arrays.equals(table, EMPLOYEES_TABLE)) {
                // Printing the values
                String employee_name = Bytes.toString(result.getValue(CF_NAME, QUAL_BYTES[0]));
                String employee_id_string = Bytes.toString(result.getValue(CF_NAME, QUAL_BYTES[1]));
                String employee_building_code = Bytes.toString(result.getValue(CF_NAME, QUAL_BYTES[2]));
                String employee_floor_number = Bytes.toString(result.getValue(CF_NAME, QUAL_BYTES[3]));
                String employee_salary = Bytes.toString(result.getValue(CF_NAME, QUAL_BYTES[4]));
                String employee_department = Bytes.toString(result.getValue(CF_NAME, QUAL_BYTES[5]));

                if (!employee_id_string.equals("employee_id")) {


                    Employee.FLOOR_NUMBER number = Employee.FLOOR_NUMBER.valueOf(employee_floor_number);

                    Employee.Builder employee = Employee.newBuilder();

                    //name, employee_id, building_code, floor_number (this should be enum), salary, department
                    employee.setName(employee_name)
                            .setEmployeeId(Integer.parseInt(employee_id_string))
                            .setBuildingCode(Integer.parseInt(employee_building_code))
                            .setFloor(number).setSalary(Integer.parseInt(employee_salary))
                            .setDepartment(employee_department);


                    String buildingCode = String.valueOf(employee.getBuildingCode());
                    String rowKey = new String(key.get());

                    /*Set the building code for a particular employee_ID*/
                    if (LookUpTable.containsKey(buildingCode)) {
                        employee.setCafeteriaCode(Integer.parseInt(LookUpTable.get(buildingCode)));
                    }

                    context.write(new LongWritable(Long.parseLong(rowKey)), new ImmutableBytesWritable(employee.build().toByteArray()));

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
