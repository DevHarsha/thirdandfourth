package com.protoassignments.fourth;

import com.protoobjects.Employee;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableSplit;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;

import java.io.IOException;
import java.util.Arrays;

public class AttendanceMapper extends TableMapper<IntWritable, ImmutableBytesWritable> {

    private byte[] EMPLOYEE_TABLE = Bytes.toBytes("employees");
    private byte[] CF_NAME = Bytes.toBytes("information");

    private byte[][] QUAL_BYTES = {"name".getBytes(), "employee_id".getBytes(),"building_code".getBytes(),
            "floor_number".getBytes(),"salary".getBytes(),
            "department".getBytes()};

    @Override
    protected void map(ImmutableBytesWritable key, Result result, Context context)  {

        TableSplit split = (TableSplit) context.getInputSplit();
        byte[] tableName = split.getTableName();

        try {
            if (Arrays.equals(tableName, EMPLOYEE_TABLE)) {
                Employee.Builder employee = Employee.newBuilder();

                String employee_name = Bytes.toString(result.getValue(CF_NAME, QUAL_BYTES[0]));
                String employee_id_string = Bytes.toString(result.getValue(CF_NAME, QUAL_BYTES[1]));
                String employee_building_code = Bytes.toString(result.getValue(CF_NAME, QUAL_BYTES[2]));
                String employee_floor_number = Bytes.toString(result.getValue(CF_NAME, QUAL_BYTES[3]));
                String employee_salary = Bytes.toString(result.getValue(CF_NAME, QUAL_BYTES[4]));
                String employee_department = Bytes.toString(result.getValue(CF_NAME, QUAL_BYTES[5]));

                Employee.FLOOR_NUMBER number;

                if (!employee_id_string.equals("employee_id")) {

                    number = Employee.FLOOR_NUMBER.valueOf(employee_floor_number);
                    employee.setName(employee_name)
                            .setEmployeeId(Integer.parseInt(employee_id_string))
                            .setBuildingCode(Integer.parseInt(employee_building_code))
                            .setFloor(number).setSalary(Integer.parseInt(employee_salary))
                            .setDepartment(employee_department);

                    int buildingCode = employee.getBuildingCode();
                    context.write(new IntWritable(buildingCode), new ImmutableBytesWritable(employee.build().toByteArray()));

                }

            }

        } catch (IllegalArgumentException | IOException | InterruptedException e) {
            e.printStackTrace();
        }

    }
}
