package com.protoassignments.fourth;

import com.protoobjects.Attendance;
import com.protoobjects.DateObject;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class BuildHashMap {

    private String INPUT = "/user/DevHarsha/ProtoSequence/attendance.seq";

    protected HashMap<Integer,Integer> getAttendanceList(){

        HashMap<Integer,Integer> map = new HashMap<>();
        Configuration conf = new Configuration();
        SequenceFile.Reader reader = null;

        try {
            Path path = new Path(INPUT);
            reader = new SequenceFile.Reader(conf, SequenceFile.Reader.file(path),
                    SequenceFile.Reader.bufferSize(4096));
            populateHashMap(map, reader);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                IOUtils.closeStream(reader);
            }
        }
        return map;

    }

    private void populateHashMap(HashMap<Integer, Integer> map, SequenceFile.Reader reader) throws IOException {

        IntWritable key = new IntWritable();
        ImmutableBytesWritable value = new ImmutableBytesWritable();

        while (reader.next(key, value)) {

            Attendance.Builder attendance =
                    Attendance.newBuilder().mergeFrom(value.get());

            int employee_attendance = 0;
            int employee_id = attendance.getEmployeeId();

            List<DateObject> list = attendance.getAttendanceListList();
            for (DateObject date : list) {
                if(date.getIsPresent()){
                    employee_attendance++;
                }
            }
            //Stores the employees and their attendance count in hashmap
            map.put(employee_id, employee_attendance);

        }

    }

}
