package com.protoassignments.fourth;

import com.protoobjects.Employee;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.HashMap;


public class AttendanceReducer extends Reducer<IntWritable, ImmutableBytesWritable, IntWritable, IntWritable>  {

    private HashMap<Integer,Integer> attendanceList;

    @Override
    protected void setup(Context context)  {
        BuildHashMap buildHashMap = new BuildHashMap();
        attendanceList = buildHashMap.getAttendanceList();
        System.out.println("Lookup table : " + attendanceList);
    }

    @Override
    protected void reduce(IntWritable key, Iterable<ImmutableBytesWritable> values, Context context) throws IOException, InterruptedException {

        int min_attendance = Integer.MAX_VALUE;
        int building_code = key.get();
        int employee_code_with_lowest_attendance = 0;

        for(ImmutableBytesWritable result : values){

            Employee.Builder employee = Employee.newBuilder().mergeFrom(result.get());
            int employee_id = employee.getEmployeeId();
            //System.out.println(employee.getEmployeeId()+" - "+employee.getBuildingCode());
            if(attendanceList.containsKey(employee_id)){

                int min_value = attendanceList.get(employee_id);
                if(min_value<min_attendance){
                    min_attendance = min_value;
                    employee_code_with_lowest_attendance = employee_id;
                }

            }

        }
        //Writes the building code and the employee_id with lowest attendance
        //System.out.println(building_code+ " - "+employee_code_with_lowest_attendance);
        context.write(new IntWritable(building_code),new IntWritable(employee_code_with_lowest_attendance));

    }

}
