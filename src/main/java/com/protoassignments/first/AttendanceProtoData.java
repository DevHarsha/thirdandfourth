package com.protoassignments.first;

import com.google.protobuf.Timestamp;
import com.protoobjects.Attendance;
import com.protoobjects.DateObject;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.io.ArrayFile;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Random;

public class AttendanceProtoData {

    private String employeeFile;
    private Path OUTPUT;
    private int NO_OF_DAYS;
    ArrayList<Attendance.Builder> attendanceList;

    FileSystem fileSystem;
    SequenceFile.Writer writer;
    Configuration conf;

    public AttendanceProtoData(String employeeFile,Path hdfsPath,int noOfDays) {
        this.employeeFile = employeeFile;
        this.OUTPUT = hdfsPath;
        this.NO_OF_DAYS = noOfDays;
        init();
    }

    private void init(){
        try {
            conf = new Configuration();
            fileSystem = FileSystem.get(conf);
            writer = getWriter(conf,OUTPUT);
            attendanceList = new ArrayList<>();
        }catch (IOException e){
            e.printStackTrace();
        }
    }


    //Function to generate random data for Attendace protooBJECTS
    public ArrayList<Attendance.Builder> populateProto() {

        ArrayList<Integer> employee_ids = getEmployeeIds();

        for(int i=0;i<employee_ids.size();i++){

            Attendance.Builder attendanceInstance = Attendance.newBuilder();
            attendanceInstance.setEmployeeId(employee_ids.get(i));
            DateObject.Builder dateStamp = DateObject.newBuilder();
            setProtoFields(attendanceInstance,dateStamp);

            attendanceList.add(attendanceInstance);
        }
        return attendanceList;

    }

    //Function to load generated data and write it to HDFS SEQUENCE FILE
    public void writeToSequenceFile() {

        int rowNum = 0;
        {
            try {
                if(!fileSystem.exists(OUTPUT)){
                    System.out.println("File already exists");
                }
                else {
                    System.out.println("File doesn't exist, creating file..");
                }

                for(Attendance.Builder attendance : attendanceList){
                    writer.append(new IntWritable((rowNum++)),
                            new ImmutableBytesWritable(attendance.build().toByteArray()));
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        try {
            fileSystem.close();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    //Function to get employee IDs from CSV file
    private ArrayList<Integer> getEmployeeIds() {

        ArrayList<Integer> employee_ids = new ArrayList<>();
        boolean columnRead = false;
        Reader reader = null;

        try {
            reader = Files.newBufferedReader(Paths.get(employeeFile));
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(reader);
            for (CSVRecord record : records) {
                if (columnRead) {
                    employee_ids.add(Integer.parseInt(record.get(1)));
                }
                columnRead = true; // skipping the header of csv file
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return employee_ids;
    }

    public SequenceFile.Writer getWriter(Configuration config, Path path) throws IOException {
        IntWritable key = new IntWritable();
        ImmutableBytesWritable value = new ImmutableBytesWritable();
        return SequenceFile.createWriter(config, SequenceFile.Writer.file(path),
                SequenceFile.Writer.keyClass(key.getClass()),
                ArrayFile.Writer.valueClass(value.getClass()));
    }

    private void setProtoFields(Attendance.Builder attendance, DateObject.Builder dateStamp) {

        Instant instant = Instant.parse("2021-05-30T18:35:24.00Z");
        for(int i=0;i<NO_OF_DAYS;i++){
            Timestamp timestamp = Timestamp.newBuilder()
                    .setSeconds(instant.getEpochSecond()).setNanos(instant.getNano()).build();
            dateStamp.setDate(timestamp);
            dateStamp.setIsPresent(new Random().nextBoolean());
            attendance.addAttendanceList(dateStamp);
        }

    }

}
